# Texts against AIs


A bibliography of articles and texts about the dangers and limits of AI use in the art and design fields.  

- [*Code is Law – On Liberty in Cyberspace*](https://framablog.org/2010/05/22/code-is-law-lessig/), Lawrence Lessig, 2000 (FR)  
- [*Web 3 and AI*](https://adactio.com/articles/20290), Jeremy Keith, 2023 (EN)  
- [*AI is killing the old web, and the new web struggles to be born*](https://www.theverge.com/2023/6/26/23773914/ai-large-language-models-data-scraping-generation-remaking-web), James Vincent, 2023 (EN)  
- [*AI is tearing Wikipedia apart*](https://www.vice.com/en/article/v7bdba/ai-is-tearing-wikipedia-apart), Claire Woodcock, 2023 (EN)  
- [*Du Web sémantique au Web synthétique*](https://affordance.framasoft.org/2023/10/du-web-semantique-au-web-synthetique/), Olivier Ertzscheid, 2023 (FR)  
- [*Penser et créer avec les IA génératives*](https://www.bortzmeyer.org/ia-generatives-colloque.html), Stéphane Botzmeyer, 2023 (FR)  
- [*Un modeste avis sur ChatGPT*](https://www.bortzmeyer.org/chatgpt.html), Stéphane Bortmeyer, 2023 (FR)  
- [*Utiliser ChatGPT peut nous rendre plus performants, mais aussi plus idiots*](https://www.ladn.eu/tech-a-suivre/chatgpt-etude-bcg-harvard-qualite-travail-consultant/), Marine Protais, 2023 (FR)  
- [*About graphic design and automation*](https://postdigitalgraphicdesign.com/interview/15/), Tancrède Ottiger, 2021 (EN)  
- [*Automated Graphic Design*](https://modesofcriticism.org/automated-graphic-design/), Fransisco Laranjo, 2016 (EN)  
- [*Automation threatens to make graphic designers obsolete*](https://eyeondesign.aiga.org/automation-threatens-to-make-graphic-designers-obsolete/), Rob Peart, 2016 (EN)  
- [*Réflexions provisoires liées aux intelligences artificielles*](https://etienne.design/2022/05/16/ai/), Étienne Mineur, 2022 (FR)  
- [*Intelligences artificielles et design dans les écoles en 2022*](https://etienne.design/2022/09/27/ai-2/), Étienne Mineur, 2022 (FR)  
- [*Artists say AI image generators are copying their style to make thousands of new images&#x00a0;&mdash; and it’s completely out of their control*](https://www.businessinsider.com/ai-image-generators-artists-copying-style-thousands-images-2022-10?r=US&IR=T), Beatrice Nolan, 2022 (EN)  
- [*Les limites de l'IA, travaux pratiques*](https://blogs.mediapart.fr/bertrand-rouzies/blog/051223/les-limites-de-l-ia-travaux-pratiques), Bertrand Rouzies, 2023 (FR)